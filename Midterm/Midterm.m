clc;
clear all;
close all;

%Main function calls
fprintf('---- Midterm ----\nThis program is a Grade Point Average calculator.\nIt will ask the user to enter their grades and return their average to them.\nIt will also determine if they achieved honours status.\n\n');
[numCourses, maxGrade] = funcInitialize();
gpa = funcA(maxGrade, numCourses);
fprintf('Final GPA: %.2f on a %d point scale\n',gpa,maxGrade);

%Function definitions
function [numCourses, maxGrade] = funcInitialize()
    maxGrade = 0;
    numCourses = 0;

    %Get the number of courses from user input
    while (~((numCourses>0)&&(numCourses<=100)))
        prompt='Please enter the total number of courses you have taken (Max 100):\n';
        numCourses = input(prompt);
        if (~((numCourses>0)&&(numCourses<=100)))
            fprintf('Sorry, that was an incorrect value, please try again.\n');
        end
    end

    %Get the maximum achievable grade point score from user
    while (~((maxGrade>=3)&&(maxGrade<=10)))
        prompt='Please enter the maximum grade point score achievable in your school''s system (must be between 3 and 10 inclusive):\n';
        maxGrade = input(prompt);
        if (~((maxGrade>=3)&&(maxGrade<=10)))
            fprintf('Sorry, that was an incorrect value, please try again. (Maximum grade must be between 3 and 10 inclusive)\n');
        end
    end
    
end

function gpa = funcA(maxGrade,numCourses)
    %Collect student grades from user based on number of courses entered
    studentGrades = zeros(1,numCourses);
    
    fprintf('\nPlease enter your grades one at a time below as numbers, pressing enter after each.\n');
    
    %Collect and validate as many grades as the user specified during initialization
    for i=1:length(studentGrades)
        prompt = strcat('#',num2str(i),':\n');
        grade = str2double(input(prompt,'s'));
        %Validate inputs 
        while(grade<0||grade>maxGrade||isnan(grade))
            fprintf('Sorry, that was an incorrect value, please try again. (Grade must be lower than the maximum achievable grade)\n');
            grade = input(prompt);
        end
        studentGrades(i) = grade;
    end
    
    %Plug values into funcB for calculations
    [gpa,result] = funcB(studentGrades,maxGrade);
    
    %Check and output result of GPA stating whether student achieved Honours or not 
    if(result==3)
        fprintf('\nStudent achieved High Honours!\n');
    elseif(result==2)
        fprintf('\nStudent achieved Honours.\n');
    else
        fprintf('\nStudent did not achieve Honours.\n');
    end
end

function [gpa,result] = funcB(studentGrades,maxGrade)
    %Calculate GPA manually (could also use the mean function)
    gpa = sum(studentGrades)/length(studentGrades);
    
    %Determine percentage grade and determine honours status
    if(gpa/maxGrade*100 > 90)
       result=3;
    elseif(gpa/maxGrade*100 > 80)
       result=2;
    else
       result=1;
    end
end