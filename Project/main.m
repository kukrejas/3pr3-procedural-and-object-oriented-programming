%This is the main driver program

clc;
clear all;
close all;

S=project_2019;
S=S.handleInputs();
S=S.initializeBots();
S=S.setBox();
S=S.handleRuntime();