classdef project_2019
   
    properties
        bots = [];
        numBots; % Number of Bots
        time;
    end
    
    properties (SetAccess = private, GetAccess = public)
      lwall=0; 
      rwall=1; 
      bwall=0; 
      twall=1;
      boxResolution;
      
      coverage; % The grid areas covered, using the coordinates.
      boxNumCovered; % Number of boxes in the grid that are covered.
      pcoverage; % Percentage of tiles covered.
       
      boxesY;
      boxesX;
      
      choice;
      targetPercent;
   end
   
   methods
       obj = handleInputs(obj);
       obj = setBox(obj);
       obj = initializeBots(obj);
       obj = updateBotPositions(obj);
       obj = handleRuntime(obj);
   end
    
    
end