function obj = updateBotPositions(obj)
    
    %Run for each bot
    for i=1:1:obj.numBots
        %Get position of individual bot
        botX = obj.bots(i).Position(1);
        botY = obj.bots(i).Position(2);
        
        %Pick a random nearby spot and make sure it's within bounds
        a = -obj.boxResolution;
        b = obj.boxResolution;

        deltax = a + (b-a).*rand(1,1);

        if (botX+deltax < obj.lwall || botX+deltax > obj.rwall)
            botX=botX-deltax;
        else
            botX=botX+deltax;
        end

        deltay = a + (b-a).*rand(1,1);

        if (botY+deltay < obj.bwall || botY+deltay > obj.twall)
            botY=botY-deltay;
        else
            botY=botY+deltay;
        end
        
        %Determine which box the bot's new spot is in, and outline in red
        x1 = obj.boxResolution - rem(botX,obj.boxResolution) + botX;
        x2 = x1 - obj.boxResolution;
        y1 = obj.boxResolution - rem(botY,obj.boxResolution) + botY;
        y2 = y1 - obj.boxResolution;
        x = [x1, x2, x2, x1, x1];
        y = [y1, y1, y2, y2, y1];
        
        %Mark box as visited in boolean tracking matrix
        boxX = round(x1/obj.boxResolution);
        boxY = round(y1/obj.boxResolution);
        obj.coverage(boxX, boxY) = true;

        [u,v] = size(obj.coverage);
        for rowNum = 1:u
            plot(x, y, 'r-','linewidth',2);
        end
        
        %Hide plotted bot in old position and plot again in new position
        obj.bots(i).Position = [botX, botY];
        set(obj.bots(i).Handler,'Visible','off');
        obj.bots(i).Handler = plot(botX,botY,'ro','markerface','g', 'markersize',12); 


        %Check for non-zeros in boolean matrix to determine percentage of boxes filled
        obj.pcoverage = nnz(obj.coverage)/(obj.boxesX*obj.boxesY)*100;
        str = ['% covered is: ',num2str(obj.pcoverage,'%.2f'),'% - Time: ',num2str(obj.time)];
        title(str);
        
    end
    xlim([obj.lwall, 1]);
    ylim([obj.bwall, 1]);
    drawnow;
end