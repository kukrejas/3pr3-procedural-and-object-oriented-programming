function obj = handleRuntime(obj)
    %If user chose time, update bots for set amount of time
    if(obj.choice==1)
        totalTime = obj.time;
        while toc < totalTime
            obj.time = round(totalTime - toc);
            obj = obj.updateBotPositions();
        end
        
        %Display results
        resultDialog([num2str(obj.pcoverage,'%.2f'),'% reached in the alloted ',num2str(totalTime),' second(s).']);
    
    %If user chose percent, update bot until percentage is reached
    else
        while obj.pcoverage < obj.targetPercent
            obj.time = round(toc);
            obj = obj.updateBotPositions();
        end
        resultDialog([num2str(obj.targetPercent,'%.2f'),'% took ',num2str(obj.time),' second(s) to reach.']);
    end
end

function resultDialog(text)
    d = dialog('Units','Normalized',...
     'Position',[.35 .45 .3 .1],...
     'Name','Finished');

    txt = uicontrol('Parent',d,...
               'Style','text',...
               'Units','Normalized',...
               'Position',[0 .35 1 .4],...
               'String',text);

    btn = uicontrol('Parent',d,...
               'Units','Normalized',...
               'Position',[.4 .2 .2 .3],...
               'String','Close',...
               'Callback','delete(gcf)');
end