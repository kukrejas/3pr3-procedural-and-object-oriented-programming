function obj = handleInputs(obj)
    %Collect user inputs
    prompt = {'Width of room (# boxes):','Length of room (# boxes):','Number of bots:'};
    dlgtitle = 'Input';
    definput = {'10','10','2'};
    opts = 'modal';
    inputs = inputdlg(prompt,dlgtitle,[1 40;1 40;1 40],definput);
    while(~(str2double(inputs{1})>0)||~(str2double(inputs{2})>0)||~(str2double(inputs{3})>0))
        waitfor(errordlg('Incorrect entries.','Input Error', opts));
        inputs = inputdlg(prompt,dlgtitle,[1 40;1 40;1 40],definput);
    end
    
    %Set object properties based on user inputs
    obj.boxesX = str2double(inputs{1});
    obj.boxesY = str2double(inputs{2});
    obj.numBots = str2num(inputs{3});
    obj.coverage = false(obj.boxesX, obj.boxesY);
    
    %Determine which side is longer and set resolution
    if (obj.boxesX > obj.boxesY)
        obj.boxResolution = 1/obj.boxesX;
        obj.twall = obj.boxResolution*obj.boxesY;
    else
        obj.boxResolution = 1/obj.boxesY;
        obj.rwall = obj.boxResolution*obj.boxesX;
    end
        
    %Get user's choice of setting time or percent
    choice = questdlg('Would you like to set the run time, or the percentage of boxes covered?', ...
        'User Prompt', ...
        'Time', 'Percentage', ...
        'Time');
    
    %Collect time or percentage input based on above choice
    switch choice
        case 'Time'
            obj.choice = 1;
            timeInput = inputdlg('Please enter the desired alloted time in seconds:',...
             'Time', [1 40])
            while(~(str2double(timeInput{1})>0))
                waitfor(errordlg('Incorrect entry.','Input Error', opts));
                timeInput = inputdlg('Please enter the desired alloted time in seconds:',...
                'Time', [1 40])
            end
            obj.time = str2num(timeInput{1});
        case 'Percentage'
            obj.choice = 2;
            percentInput = inputdlg('Please enter the desired percentage of boxes covered:',...
             'Time', [1 50])
            while(~(str2double(percentInput{1})>0)||str2double(percentInput{1})>100)
                waitfor(errordlg('Incorrect entry.','Input Error', opts));
                percentInput = inputdlg('Please enter the desired percentage of boxes covered:',...
                'Time', [1 50])
            end
            obj.targetPercent = str2double(percentInput{1});
    end
    
end