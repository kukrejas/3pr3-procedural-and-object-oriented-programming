function [obj] = setBox(obj)
    
    %Create vector for the entire X axis and Y axis
    allX=[obj.lwall:obj.boxResolution:obj.rwall];
    allY=[obj.bwall:obj.boxResolution:obj.twall];
    n=length(allX);
    m=length(allY);
    
    xlim([obj.lwall, 1]);
    ylim([obj.bwall, 1]);

    %Create columns
    for k=1:n-1

        x1=allX(k);
        x2=allX(k+1);
        
        %Create rows
        for j=1:m-1
            y1=allY(j);
            y2=allY(j+1);
            x = [x1, x2, x2, x1, x1];
            y = [y1, y1, y2, y2, y1];
            
            %Print grid based on resolution size
            plot(x, y, 'b-.','linewidth',1);
        end
    end

    %For each bot
    for i=1;1;obj.numBots
        %Get individual bot's position
        botX = obj.bots(i).Position(1);
        botY = obj.bots(i).Position(2);
        
        %Determine which box the bot is in
        x1 = obj.boxResolution - rem(botX,obj.boxResolution) + botX;
        x2 = x1 - obj.boxResolution;
        y1 = obj.boxResolution - rem(botY,obj.boxResolution) + botY;
        y2 = y1 - obj.boxResolution;
        x = [x1, x2, x2, x1, x1];
        y = [y1, y1, y2, y2, y1];
        
        %Mark box as visited in boolean tracking matrix
        boxX = round(x1/obj.boxResolution);
        boxY = round(y1/obj.boxResolution);
        obj.coverage(boxX, boxY) = true;

        %Plot red box around the box
        [u,v] = size(obj.coverage);
        for rowNum = 1:u
            plot(x, y, 'r-','linewidth',2);
        end

        %Check for non-zeros in boolean matrix to determine percentage of boxes filled
        obj.pcoverage = nnz(obj.coverage)/(obj.boxesX*obj.boxesY)*100;
        str = ['% covered is:',num2str(obj.pcoverage,'%.2f'),'%'];
        title(str);
    end
end