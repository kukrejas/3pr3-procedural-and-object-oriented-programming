function [obj] = initializeBots(obj)
    
    %Set bounds
    left = obj.lwall;
    right = obj.rwall;
    bottom = obj.bwall;
    top = obj.twall;
    
    %Initialize empty bot object array
    obj.bots = Bot.empty(0);
    
    %For each bot pick a random coordinate
    for i=1:1:obj.numBots
        obj.bots(i) = Bot;
        bx = left + (right-left).*rand(1,1);
        by = bottom + (top-bottom).*rand(1,1);
        obj.bots(i).Position = [bx, by];
        obj.bots(i).Handler = [obj.bots(i).Handler, plot(bx,by,'ro','markerface','g', 'markersize',12)];
        hold on;
    end
   
    %Commence timer
    tic;
end