#Assignment 3

Ocean wave measurement and analysis is important in Tsunami warning systems. Wave steepness is the ratio of wave height (WH) to wave length (WL) and is an indicator of wave stability. When wave steepness exceeds a 1/7 ratio the wave becomes unstable and begins to break. 

A file called **wave.txt** contains data for ocean waves. Each line in the data file contains time and wave height measurements as follows: year, month, day, hour, minute, wave height (metres), wave length (metres). The values are separated by one or more space characters.

Write a program that will create a structure to store one 
record of wave data.
After reading the data and storing all data in the structure, perform the following:
 
**(1)** Output to a file the data for all waves that exceed the 1/7 steepness ratio. Include the steepness 
ratio for each wave in the output. 

**(2)** Output to the console the average steepness for each year (2010 � 2012), and the data for the steepest wave for each year

**Submission Details:** Create a Word .doc file that contains the source code. Save this file as YourName_Assignment_3.doc and upload and submit to the appropriate AVENUE lab assignment drop-box