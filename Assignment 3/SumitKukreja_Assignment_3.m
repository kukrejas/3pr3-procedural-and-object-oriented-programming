clc;
clear all;
close all;

%Main function calls
data = readData;
wave = createStructure(data);
outputSteepWaves(wave);
outputSteepnessData(wave);

%Import data into matrix from Wave.txt file if it exists, else throw error
function data = readData()
    if (exist('Wave.txt','file'))
        data = dlmread('Wave.txt','\t');
    else
        error('Wave.txt file is missing!');
    end
end

%Parse data into structure, and calculate steepness (height / length)
function wave = createStructure(data)
    for i=1:length(data)
        wave(i).year = data(i,1);
        wave(i).month = data(i,2);
        wave(i).day = data(i,3);
        wave(i).hour = data(i,4);
        wave(i).minute = data(i,5);
        wave(i).height = data(i,6);
        wave(i).length = data(i,7);
        wave(i).steepness = (wave(i).height/wave(i).length);
    end
end

%Output waves with steepness above 1/7 to SteepWaves.txt file
function outputSteepWaves(wave)
    writetable(struct2table(wave([wave(:).steepness]>(1/7))), 'SteepWaves.txt');
end

%Output yearly data into console
function outputSteepnessData(wave)

    %Get list of 2010 values from structure
    waveList = wave([wave(:).year] == 2010);
    %Get average steepness from list
    average = mean([waveList.steepness]);
    %Get max steepness from list (and its index)
    [steepest, index] = max([waveList.steepness]);
    %Output formatted results
    fprintf(['Average steepness for 2010: %.2f\n'...
        'Highest wave in 2010:\n'],average);
    disp(waveList(index));
    
    %Get list of 2011 values from structure
    waveList = wave([wave(:).year] == 2011);
    %Get average steepness from list
    average = mean([waveList.steepness]);
    %Get max steepness from list (and its index)
    [steepest, index] = max([waveList.steepness]);
    %Output formatted results
    fprintf(['Average steepness for 2011: %.2f\n'...
        'Highest wave in 2011:\n'],average);
    disp(waveList(index));
    
    %Get list of 2012 values from structure
    waveList = wave([wave(:).year] == 2012);
    %Get average steepness from list
    average = mean([waveList.steepness]);
    %Get max steepness from list (and its index)
    [steepest, index] = max([waveList.steepness]);
    %Output formatted results
    fprintf(['Average steepness for 2012: %.2f\n'...
        'Highest wave in 2012:\n'],average);
    disp(waveList(index));
    
end