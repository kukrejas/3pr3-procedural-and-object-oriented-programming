### Question
Consider a Continuous Stirred Tank Reactor (CSTR) with an organism growing with an initial substrate concentration of $$S\scriptsize o$$ (gram per litre). Then the maximum dilution rate (per hour) for 100% yield of biomass can be obtained using 

$$D{\scriptsize max}=\mu{\scriptsize max}\bigg[1 - \sqrt \frac{K{\tiny S}}{K{\tiny S}+S{\tiny o}}\bigg]$$

Where $$\mu{\scriptsize max}$$ = maximum specific growth rate (per hour) 

$$K{\scriptsize s}$$ = saturation constant (hour per litre) 

Write a program that will calculate the maximum dilution rate in CSTR. Ask the user to enter values for $$S\scriptsize o$$ and $$\mu \scriptsize max$$ satisfying 25 ≤ $$S\scriptsize o$$ ≤ 75 and  0.2 < $$\mu\scriptsize max$$ < 0.7 , where $$S\scriptsize o$$ is an integer number and divisible by 5. If the input values are invalid, display an appropriate message and terminate the program. Saturation constant, $$K{\scriptsize s}$$ , is a randomly generated integer such that 2 ≤ $$K{\scriptsize s}$$ ≤ 7. Calculate maximum dilution rate and print your result. If the dilution rate is in the range, 0.35 < $$D{\scriptsize max}$$ < 0. 45 , display a message stating that *“Kinetic parameters are acceptable”* otherwise display *“Kinetic parameters are not acceptable”*. Don’t forget to print all input values. 

**Submission Requirements:**  A matlab file named *“firstName_Lastname_A01.m”* that has the code for the program. 