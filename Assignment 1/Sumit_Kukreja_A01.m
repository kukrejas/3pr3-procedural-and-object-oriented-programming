clc;
clear all;
close all;

%Get Initial Substrate Concentration value from user
prompt='Please enter an initial substrate concentration value between 25 and 75 (inclusive) that is divisible by 5:\n';
initSub = input(prompt)

%Validate input and terminate if necessary
if ( (~((initSub>=25)&&(initSub<=75))) || (rem(initSub,5)~=0) )
    fprintf('Sorry, that was an incorrect value, terminating program.\n');
    return
end

%Get Maximum specific growth rate from user
prompt='Please enter a maximum specific growth rate between 0.2 and 0.7:\n';
maxGrowth = input(prompt)

%Validate input and terminate if necessary
if (~((maxGrowth>0.2)&&(maxGrowth<0.7)))
    fprintf('Sorry, that was an incorrect value, terminating program.\n');
    return
end

%Generate random Saturation Constant value between 2 and 7
satConstant = randi([2,7])

%Calculate Maximum Dilution rate based on inputs and saturation constant
maxDilution = maxGrowth * (1 - sqrt(satConstant / (satConstant + initSub)));

%Print inputs and randomly generated constant
fprintf('Initial Substrate Concentration: %d\nMaximum Specific Growth Rate: %.2f\nSaturation Constant: %d\nMaximum Dilution = %.2f\n', initSub, maxGrowth, satConstant, maxDilution);

%Determine if dilution rate is acceptable and print result
if (maxDilution>0.35&&maxDilution<0.45)
    fprintf('Kinetic parameters are acceptable.\n')
else 
    fprintf('Kinetic parameters are not acceptable.\n')
end