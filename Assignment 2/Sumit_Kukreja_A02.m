clc;
clear all;
close all;

%Main function calls
fprintf('---- Question #1 ----\n');
[g,t] = inputValues();
d = calculateDistance(g,t);
displayResults(g,t,d);
fprintf('\n---- Question #2 ----');
getAnswers();



function [g,t] = inputValues()
    g = -1;
    t = -1;

    %Get the amount of time the object fell from user
    while (~((t>=0)&&(t<=45)))
        prompt='Please enter how long the object fell in seconds (must be between 0s and 45s inclusive):\n';
        t = input(prompt);
        if (~((t>=0)&&(t<=45)))
            fprintf('Sorry, that was an incorrect value, please try again.\n');
        end
    end

    %Get the gravity value from user
    while (~((g>=0.1)&&(g<=10.2)))
        prompt='Please enter the value of gravity affecting the object (must be between 0.1 and 10.2 inclusive):\n';
        g = input(prompt);
        if (~((g>=0.1)&&(g<=10.2)))
            fprintf('Sorry, that was an incorrect value, please try again.\n');
        end
    end
    
end

function d = calculateDistance(g,t)
    %Calculate distance based on input gravity and time
    d = (1/2) * g * t * t;
end

function displayResults(g,t,d)
    %Print input gravity and time values, and calculated distance
    fprintf('\nGravity: %.1f\nTime: %d\nDistance: %.2f\n', g, t, d);
end

function getAnswers()
    %Declare answers and fill two empty arrays for inputs and results
    answerKey = ['B' 'D' 'B' 'A' 'C' 'B' 'C' 'A' 'A' 'D' 'B' 'C' 'A' 'A' 'C' 'C' 'D' 'C' 'B' 'A'];
    studentAns = zeros(1,20);
    result = zeros(1,20);
    
    fprintf('\nPlease enter student answers below.\n');
    
    %Collect answers from user and record in result if they match answer key
    for i=1:20
        studentAns(i) = upper(input(sprintf('#%d:\n',i),'s'));
        if(studentAns(i)==answerKey(i))
            result(i) = 1;
        end
    end
    
    %Check if student passed
    if(sum(result) >= 15)
        fprintf('\nStudent passed.\n');
    else 
        fprintf('\nStudent failed.\n');
    end
    
    fprintf('%d/20 questions answered correctly.',sum(result));
    
    %Find incorrect answers
    incorrect = find(result==0);
    fprintf('\nQuestions answered incorrectly (Total %d):\n', length(incorrect));
    disp(incorrect);
end