# Assignment 2

### Question 1 
Write a matlab program that will compute the distance (D) an object falls in a specific amount of time (t) using the formula 
$$D = \frac{1}{2}gt^2$$

The user must input the amount of time the object falls (t, measured in sec.) and the gravity (g, measured in m/sec2). The values of t and g must satisfy the following criteria: **0 ≤ 𝑡 ≤ 45, 0.1 ≤ 𝑔 ≤ 10.2**. Your program should run as follows. The user input for t and g should be via the function named **inputValues**. The input validation must happen inside this function. The function named **calculateDistance** should be calculating the value of D. The function named **displayResults** should be displaying the values of t, g and D. 

**You cannot use global variables in your program.**

### Question 2
The local driver’s license office has asked you to design a matlab program that grades the written portion of the driver’s license exam. The exam has 20 multiple choice questions whose correct answers are as follows: 

$$\begin{matrix}
   1.B & 6.B & 11.B & 16.C \\
   2.D & 7.C & 12.C & 17.D \\
   3.B & 8.A & 13.A & 18.C \\
   4.A & 9.A & 14.A & 19.B \\
   5.C & 10.D & 15.C & 20.A 
\end{matrix}$$

Your program should store these correct answers in an array. In a function named **getAnswers** program should ask the user to enter the student’s answers for each of the 20 questions, which should be stored in another array. After the student’s answers have been entered, the program should display a message indicating whether the student passed or failed the exam. (A student must correctly answer 15 of the 20 questions to pass the exam.) It should then display the total number of correctly answered questions, the total number of incorrectly answered questions, and a list showing the question numbers of the incorrectly answered questions. 

**You cannot use global variables in your program.**

**Submission Requirements**:  A **single** word file named *“firstName_Lastname_A02”* that has the code for both the programs should be submitted to the dropbox. 